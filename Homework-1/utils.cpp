#include "utils.h"
#include "stack.h"
#include "iostream"

void reverse(int* nums, unsigned int size) {
	/**
	 * @brief reverses a given array
	 * @param nums pointer to an array
	 * @param size array size
	*/
	stack* s = new stack;
	initStack(s);

	for (int i = 0; i < size; i++) {
		push(s, nums[i]);
	}
	for (int i = 0; i < size; i++) {
		nums[i] = pop(s);
	}
}

int* reverse10() {
	/**
	 * @brief creates an array and returns it reversed
	 * @return pointer to a created array
	*/
	int size = 10;
	int* arr = new int[size];

	for (int i = 0; i < size; i++)
	{
		std::cout << "enter number (i = " << i << "): ";
		std::cin >> arr[i];
	}
	reverse(arr, size);

	return arr;
}


