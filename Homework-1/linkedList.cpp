#include "linkedList.h"
#include <iostream>

void add(Node** head, unsigned int newValue) {
	/**
	 * @brief adds new node to the head
	 * @param head pointer
	 * @param newValue 
	*/
	Node* newHead = new Node;
	newHead->value = newValue;

	newHead->next = *head;
	*head = newHead;
}

Node* remove(Node* head) {
	/**
	 * @brief removes the head
	 * @param head pointer
	 * @return 
	*/
	if (head == NULL) {
		return NULL;
	}
	
	Node* temp = head;
	head = head->next;

	delete temp;
	return head;
}

void deleteList(Node** head) {
	/**
	 * @brief detetes the whole linked list
	 * @param head pointer
	*/
	Node* current = *head;
	Node* next;

	while (current != NULL)
	{
		next = current->next;
		delete current;
		current = next;
	}

	*head = NULL;
}

