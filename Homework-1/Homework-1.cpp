// Homework-1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "queue.h"
#include "linkedList.h"
#include "stack.h"
#include "utils.h"
#include "bonus.h"

int main()
{
    ///////////////////////////////////////////////////////////////////////////////////
    //QUEUE

    /*queue* q = new queue;

    enqueue(q, 1);
    enqueue(q, 2);
    enqueue(q, 3);
    enqueue(q, 5);
    enqueue(q, 6); 

    std::cout << "\nelements: \n";
    printQueue(q);
    
    std::cout << "\nfirst element: \n";
    std::cout << dequeue(q) << std::endl;

    std::cout << "\nfirst element: \n";
    std::cout << dequeue(q) << std::endl;
    
    std::cout << "\nelements: \n";
    printQueue(q);

    cleanQueue(q);*/

    ///////////////////////////////////////////////////////////////////////////////////
    //BONUS

   /* queue* q = new queue;
    initQueue(q, 100);

    std::cout << q->size << std::endl;
    std::cout << q->first << std::endl;
    std::cout << q->last << std::endl;
    
    enqueue(q, 1);
    enqueue(q, 2);
    enqueue(q, 3);
    enqueue(q, 4);
    enqueue(q, 5);
    enqueue(q, 6);
    enqueue(q, 7);
    enqueue(q, 8);
    enqueue(q, 9);
    enqueue(q, 10);
    enqueue(q, 11);

    std::cout << "\nelements: \n";
    printQueue(q);

    std::cout << "\nfirst element: \n";
    std::cout << dequeue(q) << std::endl;

    std::cout << "\nfirst element: \n";
    std::cout << dequeue(q) << std::endl;

    std::cout << "\nfirst element: \n";
    std::cout << dequeue(q) << std::endl;

    std::cout << "\nfirst element: \n";
    std::cout << dequeue(q) << std::endl;

    std::cout << "\nelements: \n";
    printQueue(q);

    cleanQueue(q);*/

    ///////////////////////////////////////////////////////////////////////////////////
    //LINKED LIST

   /* Node* head = NULL;

    add(&head, 1);
    add(&head, 2);
    add(&head, 3);
    add(&head, 5);
    add(&head, 6);

    std::cout << "something:\n";
    

    for (Node* temp = head; temp != NULL; temp = temp->next)
        std::cout << temp->value << " ";

    std::cout << std::endl;

    head = remove(head);

    for (Node* temp = head; temp != NULL; temp = temp->next)
        std::cout << temp->value << " ";*/

    ///////////////////////////////////////////////////////////////////////////////////
    //STACK
    
    /*stack* stck = new stack;
    initStack(stck);

    std::cout << "count: " << stck->count << "\n";

    push(stck, 1);
    push(stck, 2);
    push(stck, 3);
    push(stck, 4);
    push(stck, 6);
    push(stck, 7);

    for (Node* temp = stck->head; temp != NULL; temp = temp->next)
        std::cout << temp->value << " "; 

    std::cout << "\n";
    std::cout << "count: " << stck->count << "\n";
    
    cleanStack(stck);

    for (int i = 0; i < 10; i++)
    {
        std::cout << pop(stck) << std::endl;
    }*/

    ///////////////////////////////////////////////////////////////////////////////////
    //UTIL REVERSE

    //int arr[] = { 1, 2, 3, 4, 5, 6 };
    //int size = std::size(arr);
    //std::cout << "size: " << size << std::endl;
    //
    //for (int i = 0; i < size; i++) {
    //    std::cout << "arr[" << i << "] = " << arr[i] << std::endl;
    //}
    //std::cout << std::endl;


    //reverse(arr, size);
    //for (int i = 0; i < size; i++) {
    //    std::cout << "arr[" << i << "] = " << arr[i] << std::endl;
    //}
    //std::cout << std::endl;

    ///////////////////////////////////////////////////////////////////////////////////
    //UTIL REVERSE10

    /*int* reversedArray = reverse10();
    const int size = 10;

    for (int i = 0; i < size; i++) {
        std::cout << "arr[" << i << "] = " << reversedArray[i] << std::endl;
    }
    std::cout << std::endl;*/

    return 0;
}

