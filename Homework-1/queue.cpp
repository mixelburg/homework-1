#include "queue.h"
#include <algorithm>
#include <iterator>
#include <iostream>

void initQueue(queue* q, unsigned int size) {
	/**
	 * @brief initializes queue
	 * @param q pointer to a queue
	 * @param size size of the queue
	*/
	q->elements = new int[size];
}

void cleanQueue(queue* q) {
	/**
	 * @brief creans the memory
	 * @param q pointer to a queue
	*/
	delete[] q->elements;
	q->size = NULL;
}

void enqueue(queue* q, unsigned int newValue) {
	/**
	 * @brief adds element to the end
	 * @param q pointer to a queue
	 * @param newValue 
	*/
	int* temp = new int[q->size + 1];
	std::copy(q->elements, q->elements + q->size, temp);

	q->elements = temp;
	q->elements[q->size] = newValue;

	q->size++;
}

int dequeue(queue* q) {
	/**
	 * @brief removes element from the start of the queue
	 * @param q pointer to the queue
	 * @return value at the start 
	*/
	if (q->size == 0) {
		return -1;
	}

	int firstElement = q->elements[0];
	int* temp = new int[q->size - 1];
	std::copy(q->elements + 1, q->elements + q->size, temp);
	q->elements = temp;

	q->size--;

	return firstElement;
}// return element in top of queue, or -1 if empty

void printQueue(queue* q) {
	/**
	 * @brief prints the queue
	 * @param q pinter to the queue
	*/
	for (int i = 0; i < q->size; i++)
	{
		std::cout << q->elements[i] << std::endl;
	}
}
