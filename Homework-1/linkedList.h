#ifndef LINKEDLIST_H
#define LINKEDLIST_H


/* a queue contains positive integer values. */
typedef struct Node{
	int value;
	Node* next;
} Node;

void add(Node** head, unsigned int newValue);
Node* remove(Node* head);
void deleteList(Node** head);

#endif