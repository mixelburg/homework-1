//#include "bonus.h"
//#include <iostream>
//
//void initQueue(queue* q, unsigned int size) {
//	/**
//	 * @brief initializes the queue
//	 * @param q pointer to a queue
//	 * @param size size of the queue
//	*/
//	q->elements = new int[size];
//	q->size = size;
//	q->first = 0;
//	q->last = 0;
//}
//void cleanQueue(queue* q) {
//	/**
//	 * @brief creans the memory
//	 * @param q pointer to a queue
//	*/
//	delete[] q->elements;
//	q->size = NULL;
//	q->first = NULL;
//	q->last = NULL;
//}
//
//void enqueue(queue* q, unsigned int newValue) {
//	/**
//	 * @brief adds element to the end of the queue
//	 * @param q pointer to a queue
//	 * @param newValue 
//	*/
//	if (q->last == q->size - 1) {
//		std::cout << "Queue overflow" << std::endl;
//	}
//	else {
//		q->elements[q->last] = newValue;
//		q->last++;
//	}
//}
//
//int dequeue(queue* q) {
//	/**
//	 * @brief removes element from the beginning of the queue and returns it (returns -1 if queue is empty)
//	 * @param q pointer to a queue
//	 * @return first element (or -1)
//	*/
//	if (q->first > q->last) {
//		return -1;
//	}
//	else {
//		q->first++;
//		return q->elements[q->first - 1];
//	}
//}
//
//void printQueue(queue* q) {
//	/**
//	 * @brief prints the queue
//	 * @param q pinter to the queue
//	*/
//	for (int i = q->first; i < q->last; i++)
//	{
//		std::cout << q->elements[i] << std::endl;
//	}
//};
